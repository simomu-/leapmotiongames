﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class FingerInputModule : BaseInputModule{

	public static Vector2 FingerPosition;
	public static bool IsPoint = false;

	PointerEventData m_FingerPointerEventData;
	[SerializeField] LeapHandInputManager m_InputManager;
	[SerializeField] Image m_FingerPointImage;
	[SerializeField] Image m_LoadImage;

	bool m_OnPoint = false;
	bool m_OnEnter = false;

	void Update(){
		Vector3 pos;
		if (m_InputManager.OnPoint (out pos)) {
			IsPoint = true;
			FingerPosition = pos;
		} else {
			IsPoint = false;
		}
	}

	PointerEventData GetFingerPointerEventData(){
		if (m_FingerPointerEventData == null) {
			m_FingerPointerEventData = new PointerEventData (eventSystem);
		}
		m_FingerPointerEventData.Reset ();
		m_FingerPointerEventData.delta = Vector2.zero;
		m_FingerPointerEventData.position = FingerPosition;
		m_FingerPointerEventData.scrollDelta = Vector2.zero;
		eventSystem.RaycastAll (m_FingerPointerEventData, m_RaycastResultCache);
		m_FingerPointerEventData.pointerCurrentRaycast = FindFirstRaycast (m_RaycastResultCache);
		m_RaycastResultCache.Clear ();
		return m_FingerPointerEventData;

	}

	private bool SendUpdateEventToSelectedObject(){
		if (eventSystem.currentSelectedGameObject == null) {
			return false;
		}
		BaseEventData data = GetBaseEventData ();
		ExecuteEvents.Execute (eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);
		return data.used;
	}

	public override void Process() {
		// send update events if there is a selected object - this is important for InputField to receive keyboard events
		SendUpdateEventToSelectedObject();
		PointerEventData lookData = GetFingerPointerEventData();
		// use built-in enter/exit highlight handler
		HandlePointerExitAndEnter(lookData,lookData.pointerCurrentRaycast.gameObject);

		m_FingerPointImage.gameObject.SetActive (IsPoint);
		m_FingerPointImage.transform.position = FingerPosition;
		if (lookData.pointerCurrentRaycast.gameObject != null && !m_OnPoint && IsPoint) {
			StartCoroutine (PointerTimeCount ());
		} else {
			m_LoadImage.fillAmount = 0;
			if (m_OnPoint && !IsPoint) {
				m_OnPoint = false;
				m_OnEnter = false;
				StopAllCoroutines ();
			}
		}

		if (m_OnEnter) {
			if (lookData.pointerCurrentRaycast.gameObject != null) {
				GameObject go = lookData.pointerCurrentRaycast.gameObject;
				GameObject newPressed = ExecuteEvents.ExecuteHierarchy (go, lookData, ExecuteEvents.submitHandler);
				if (newPressed == null) {
					// submit handler not found, try select handler instead
					newPressed = ExecuteEvents.ExecuteHierarchy (go, lookData, ExecuteEvents.selectHandler);
				}
				if (newPressed != null) {
					eventSystem.SetSelectedGameObject (newPressed);
				}
			}
		}
		/*
		if (eventSystem	.currentSelectedGameObject && controlAxisName != null && controlAxisName != "") {
			float newVal = Input.GetAxis (controlAxisName);
			if (newVal > 0.01f || newVal < -0.01f) {
				AxisEventData axisData = GetAxisEventData(newVal,0.0f,0.0f);
				ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, axisData, ExecuteEvents.moveHandler);
			}
		}*/
	}

	IEnumerator PointerTimeCount(){
		m_OnPoint = true;
		m_LoadImage.fillAmount = 0;
		for (float i = 0; i < 1f; i += Time.deltaTime) {
			m_LoadImage.fillAmount = i / 1f;
			yield return null;
		}
		m_OnEnter = true;
		yield return null;
		m_OnEnter = false;
		m_OnPoint = false;
	}
		
}
