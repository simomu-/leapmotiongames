﻿using UnityEngine;
using System.Collections;

public class PointObject : MonoBehaviour {

	[SerializeField] float m_Point;
	[SerializeField,Range (0,1f)] float m_HighPointProbability;
	bool m_IsHighPoint = false;

	void Start(){
		Material material = GetComponent<Renderer> ().material;
		float p = Random.Range (0, 1f);
		Color c = p <= m_HighPointProbability ? Color.red : Color.yellow;
		m_IsHighPoint = p <= m_HighPointProbability ? true : false;
		material.EnableKeyword ("_EMISSION");
		material.SetColor ("_Color", c);
		material.SetColor ("_EmissionColor",c);
	}


	void OnTriggerEnter(Collider _other){
		if (_other.transform.root.tag == "Player") {
			float point = m_IsHighPoint ? m_Point * 2 : m_Point;
			ScoreCounter.Instance.SetScore (point);
			GetComponent<Collider> ().enabled = false;
			Destroy (gameObject);
		}
	}

}
