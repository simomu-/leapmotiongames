﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Title : MonoBehaviour {

	void Start(){
		GameManager.Instance.RecentPlayStageIndex = 0;
	}

	public void GameStart(){
		//SceneManager.LoadScene ("GameSelect");
		GameManager.Instance.SceneChange("GameSelect");
	}

	public void Quit(){
		GameManager.Instance.Blackout(2f,1f,Color.black);
		Invoke ("QuitInvoke", 1f);
	}

	void QuitInvoke(){
		Application.Quit ();
	}

}
