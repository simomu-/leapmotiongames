﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class LeapCarControl : MonoBehaviour {

	[SerializeField] LeapHandInputManager m_LeapInputR;
	[SerializeField] CarUserControl m_CarControl;

	void Start(){
		GameManager.Instance.RecentPlayStageIndex = 1;
	}

	void Update(){
		m_CarControl.Horizontal = m_LeapInputR.GetLeapHorizontalAxis ();
		m_CarControl.Vertical = m_LeapInputR.GetLeapVerticalAxis ();
	}
}
