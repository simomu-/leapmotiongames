﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using System;

public class UIManager : MonoBehaviour {

	public static UIManager Instance;

	[SerializeField] LeapHandInputManager m_LeapInputR;
	[SerializeField] Animator m_MenuAnimator;
	[SerializeField] Text m_TimeText;
	[SerializeField] Text m_ScoreText;
	[SerializeField] Text m_MessageText;

	readonly string StageSelectSceneName = "GameSelect";
	bool m_IsButtonIn = false;

	void Awake(){
		Instance = this;
	}

	void Update(){
		if (m_LeapInputR.OnFlip (FlipDirection.Down) && !m_IsButtonIn) {
			m_MenuAnimator.SetTrigger ("ButtonIn");
			m_IsButtonIn = true;
		}
		if (m_LeapInputR.OnFlip (FlipDirection.Up)) {
			MenuClose ();
		}
	}

	public void MenuClose(){
		if (m_IsButtonIn) {
			m_MenuAnimator.SetTrigger ("ButtonOut");
			m_IsButtonIn = false;
		}
	}

	public void BackStageSelect(){
		GameManager.Instance.SceneChange (StageSelectSceneName, 1f, Color.black);
	}

	/*
	public void SetTimeCount(float time){
		float t = ((int)(time * 10)) / 10.0f;
		m_ScoreText.text = String.Format ("Time:{00}", time);
	}
	*/

	public void SetCurrentTime(float currentTime){
		m_TimeText.text = (int)currentTime / 60 + ":" + (int)currentTime % 60; 
		int millisecond = (int)Mathf.Floor((currentTime - Mathf.Floor(currentTime)) * 100)	;
		m_TimeText.text = System.String.Format ("{0:00}:{1:00}:{2:00}", (int)currentTime / 60, (int)currentTime % 60,millisecond);
	}

	public void SetScore(float value){
		m_ScoreText.text = ((int)value).ToString ();
	}

	public void SetMessageText(string text, Color color, bool enable){
		m_MessageText.gameObject.SetActive (enable);
		m_MessageText.color = color;
		m_MessageText.text = text;
	}




}
