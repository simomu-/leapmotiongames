﻿using UnityEngine;
using System.Collections;

public class UIControllTest : MonoBehaviour {

	[SerializeField] LeapHandInputManager m_RightLeapHand;
	[SerializeField] LeapHandInputManager m_LeftLeapHand;
	[SerializeField] StageSelectScroll m_UIScroller;
	[SerializeField] Animator[] m_SideUIAnimators;

	bool m_SideButtonVisivle = false;

	void Update(){
		if (m_RightLeapHand.OnFlip (FlipDirection.Right) || m_LeftLeapHand.OnFlip (FlipDirection.Right)) {
			m_UIScroller.MoveLeft ();
		}
		if (m_RightLeapHand.OnFlip (FlipDirection.Left) || m_LeftLeapHand.OnFlip (FlipDirection.Left)) {
			m_UIScroller.MoveRight ();
		}
		if ((m_RightLeapHand.OnFlip (FlipDirection.Down) || m_LeftLeapHand.OnFlip (FlipDirection.Down)) && !m_SideButtonVisivle) {
			for (int i = 0; i < m_SideUIAnimators.Length; i++) {
				m_SideButtonVisivle = true;
				m_SideUIAnimators [i].SetTrigger ("ButtonIn");
			}
		}
		if ((m_RightLeapHand.OnFlip (FlipDirection.Up) || m_LeftLeapHand.OnFlip (FlipDirection.Up)) && m_SideButtonVisivle) {
			for (int i = 0; i < m_SideUIAnimators.Length; i++) {
				m_SideButtonVisivle = false;
				m_SideUIAnimators [i].SetTrigger ("ButtonOut");
			}
		}

		Vector3 fingerPosition;
		FingerInputModule.IsPoint = m_RightLeapHand.OnPoint (out fingerPosition);
		FingerInputModule.FingerPosition = fingerPosition;

	}
}
