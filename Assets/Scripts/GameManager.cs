﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : SingletonMonoBehaviour<GameManager> {

	[HideInInspector] public bool IsStagePlaying = false;
	[HideInInspector] public bool IsSceneChangeing = false;
	public readonly string StageScelectSceneName = "";

	[SerializeField] Image m_FadeImage;
	bool IsFadeOut = false;
	bool m_IsPause = false;
	int m_RecentPlayStageIndex = 0;
	float m_BGMVolume = 0.2f;
	float m_SEVolume = 0.9f;
	float m_CarHighScore = 0;
	float m_AeroplaneHighScore = 0;
	float m_GunShootingHighScore = 0;

	public float CarHighScore{
		set{
			if (m_CarHighScore == 0) {
				m_CarHighScore = value;
				PlayerPrefs.SetFloat ("CarHighScore", m_CarHighScore);
			} else {
				m_CarHighScore = PlayerPrefs.GetFloat ("CarHighScore");
				if (m_CarHighScore >= value) {
					m_CarHighScore = value;
					PlayerPrefs.SetFloat ("CarHighScore", m_CarHighScore);
				}
			}
		}
		get{
			m_CarHighScore = PlayerPrefs.GetFloat ("CarHighScore");
			return m_CarHighScore;
		}
	}

	public float AeroplaneHighScore{
		set{
			m_AeroplaneHighScore = PlayerPrefs.GetFloat ("AeroplaneHighScore");
			if(m_AeroplaneHighScore <= value){
				m_AeroplaneHighScore = value;
				PlayerPrefs.SetFloat ("AeroplaneHighScore", m_AeroplaneHighScore);
			}
		}
		get{
			m_AeroplaneHighScore = PlayerPrefs.GetFloat ("AeroplaneHighScore");
			return m_AeroplaneHighScore;
		}
	}

	public float GunShootingHighScore{
		set{
			m_GunShootingHighScore = PlayerPrefs.GetFloat ("GunShootingHighScore");
			if (m_GunShootingHighScore <= value) {
				m_GunShootingHighScore = value;
				PlayerPrefs.SetFloat ("GunShootingHighScore", m_GunShootingHighScore);
			}
		}
		get{
			m_GunShootingHighScore = PlayerPrefs.GetFloat ("GunShootingHighScore");
			return m_GunShootingHighScore;
		}
	}

	public bool IsPause{
		set{
			m_IsPause = value;
			Time.timeScale = m_IsPause ? 0f : 1f;
		}
		get{
			return m_IsPause;
		}
	}

	public int RecentPlayStageIndex{
		set{
			m_RecentPlayStageIndex = value;
		}
		get{
			return m_RecentPlayStageIndex;
		}
	}

	public float BGMVolume{
		set{ m_BGMVolume = Mathf.Clamp01 (value); }
		get{ return m_BGMVolume; }
	}

	public float SEVolume{
		set{ m_SEVolume = Mathf.Clamp01 (value);}
		get{ return m_SEVolume;}
	}
		

	public void SceneChange(string sceneName){
		if(!IsSceneChangeing){
			StartCoroutine (SceneChangeCoroutine (sceneName,1f,Color.black));
		}
	}

	public void SceneChange(string sceneName,float time){
		if(!IsSceneChangeing){
			StartCoroutine (SceneChangeCoroutine (sceneName,time,Color.black));
		}
	}

	public void SceneChange(string sceneName, float time, Color color){
		if(!IsSceneChangeing){
			StartCoroutine (SceneChangeCoroutine (sceneName,time,color));
		}
	}
		

	IEnumerator SceneChangeCoroutine(string sceneName,float time,Color color){
		IsSceneChangeing = true;
		m_FadeImage.gameObject.SetActive (true);
		color.a = 0;
		m_FadeImage.color = color;
		float t = 0;
		float deltaAlpha = 1f / (time / 2);
		Color c = m_FadeImage.color;
		while(t <= time / 2f){
			c.a += deltaAlpha * Time.deltaTime;;
			m_FadeImage.color= c;
			t += Time.deltaTime;
			yield return null;
		}

		t = 0;
		c = m_FadeImage.color;
		SceneManager.LoadScene (sceneName);
		yield return null;

		while(t <= time / 2f){
			c.a -= deltaAlpha * Time.deltaTime;
			m_FadeImage.color = c;
			t += Time.deltaTime;
			yield return null;
		}
		m_FadeImage.gameObject.SetActive (false);
		IsSceneChangeing = false;
	}

	public void Blackout(float time,float stopTime,Color color){
		if (!IsFadeOut) {
			StartCoroutine (BlackoutCoroutine (time,stopTime,color));
		}
	}

	IEnumerator BlackoutCoroutine(float time,float stopTime,Color color){
		IsFadeOut = true;
		m_FadeImage.gameObject.SetActive (true);
		color.a = 0;
		m_FadeImage.color = color;
		float t = 0;
		float deltaAlpha = 1f / (time / 2);
		Color c = m_FadeImage.color;
		while(t <= time / 2f){
			c.a += deltaAlpha * Time.deltaTime;;
			m_FadeImage.color= c;
			t += Time.deltaTime;
			yield return null;
		}
		t = 0;
		c = m_FadeImage.color;
		if (stopTime <= 0)  {
			yield return null;
		} else {
			yield return new WaitForSeconds(stopTime);
		}
		while(t <= time / 2f){
			c.a -= deltaAlpha * Time.deltaTime;
			m_FadeImage.color = c;
			t += Time.deltaTime;
			yield return null;
		}
		m_FadeImage.gameObject.SetActive (false);
		IsFadeOut = false;
	}

}
