﻿using UnityEngine;
using System.Collections;
using Leap;
using Leap.Unity.PinchUtility;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PinchTest : MonoBehaviour {

	[SerializeField] LeapPinchDetector m_LeapPinch;
	[SerializeField] LeapHandInputManager m_InputManager_R;

	Transform m_PinchWindowTrans;

	void Update(){
		Debug.Log (m_LeapPinch.IsPinching);
		Vector3 fingerPos;
		if (m_InputManager_R.OnPoint (out fingerPos)) {
			FingerInputModule.FingerPosition = fingerPos;
			FingerInputModule.IsPoint = true;
		} else {
			FingerInputModule.IsPoint = false;
		}

		if (m_LeapPinch.DidStartPinch) {
			Debug.Log ("Pinch Start");
			Vector3 pos;
			m_InputManager_R.OnPoint (out pos);
			Ray ray = Camera.main.ScreenPointToRay (pos);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 100f)) {
				m_PinchWindowTrans = hit.transform;
				Debug.Log (m_PinchWindowTrans);
				Debug.Log ("Window hit");
			}
		}

		if (m_LeapPinch.IsPinching && m_PinchWindowTrans != null) {
			m_PinchWindowTrans.position = Camera.main.WorldToScreenPoint (m_LeapPinch.Position);
			Debug.Log ("Window Move");
		}

		if (m_LeapPinch.DidEndPinch) {
			m_PinchWindowTrans = null;
		}
	}

}
