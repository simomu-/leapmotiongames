﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleUIManager : MonoBehaviour {

	[SerializeField] StageSelectScroll m_StageScroll;
	[SerializeField] Text[] m_HighScoreText;

	void Start(){
		m_StageScroll.StageIndex = GameManager.Instance.RecentPlayStageIndex;
		SetHighScore (0, GameManager.Instance.AeroplaneHighScore);
		SetHighScore (1, GameManager.Instance.CarHighScore);
		SetHighScore (2, GameManager.Instance.GunShootingHighScore);
	}

	public void ResetHightScore(){
		PlayerPrefs.DeleteAll ();
		SetHighScore (0, GameManager.Instance.AeroplaneHighScore);
		SetHighScore (1, GameManager.Instance.CarHighScore);
		SetHighScore (2, GameManager.Instance.GunShootingHighScore);
	}

	public void SetHighScore(int index,float score){
		if (index == 1) {
			int millisecond = (int)Mathf.Floor ((score - Mathf.Floor (score)) * 100);
			m_HighScoreText [index].text = System.String.Format ("HighScore {0:00}:{1:00}:{2:00}", (int)score / 60, (int)score % 60, millisecond);
		} else {
			m_HighScoreText [index].text = "HighScore:" + (int)score;
		}
	}

	public void SceneChange(string sceneName){
		GameManager.Instance.SceneChange (sceneName, 1f, Color.black);
	}

	public void Quit(){
		//Application.Quit ();
		GameManager.Instance.Blackout(2f,1f,Color.black);
		Invoke ("QuitInvoke", 1f);
	}

	void QuitInvoke(){
		Application.Quit ();
	}


}
