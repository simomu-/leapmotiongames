﻿using UnityEngine;
using UnityEngine.UI;

public class StageSelectScroll : MonoBehaviour {

	[SerializeField] ScrollRect m_ScrollRect;
	[SerializeField] RectTransform m_Content;
	[SerializeField] float m_MoveSpeed = 10f;
	int m_StageIndex = 0;
	int m_MaxStageCount;

	public int StageIndex{
		set{
			if (value >= 0 && value < m_Content.childCount) {
				m_StageIndex = value;
				m_ScrollRect.horizontalNormalizedPosition = (float) m_StageIndex / m_MaxStageCount;
			}
		}
		get{
			return m_StageIndex;
		}
	}

	void Awake () {
		m_MaxStageCount = m_Content.childCount - 1;
	}
	
	void Update () {
		float position = (float)m_StageIndex / m_MaxStageCount;
		m_ScrollRect.horizontalNormalizedPosition = Mathf.Lerp (m_ScrollRect.horizontalNormalizedPosition, position, m_MoveSpeed * Time.deltaTime);
	}

	public void MoveRight(){
		if (m_StageIndex < m_MaxStageCount) {
			m_StageIndex++;
		}
	}

	public void MoveLeft(){
		if (m_StageIndex > 0) {
			m_StageIndex--;
		}
	}
}
