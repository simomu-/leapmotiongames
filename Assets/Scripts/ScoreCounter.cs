﻿using UnityEngine;
using System.Collections;

public class ScoreCounter : MonoBehaviour {

	public static ScoreCounter Instance;

	enum StageMode{
		Aeroplane,GunShooting
	}

	readonly string m_StageScelectSceneName = "GameSelect";

	[SerializeField] float m_TimeLimit;
	[SerializeField] StageMode m_StageMode;
	float m_CurrentTime;
	float m_CurrentScore = 0;
	bool m_IsTimeCount = true;

	void Awake(){
		Instance = this;
		m_CurrentTime = m_TimeLimit;
	}

	void Update(){
		if (m_IsTimeCount) {
			m_CurrentTime -= Time.deltaTime;
			if (m_CurrentTime <= 0) {
				m_CurrentTime = 0;
				m_IsTimeCount = false;
				if (!IsInvoking ()) {
					if (m_StageMode == StageMode.Aeroplane) {
						GameManager.Instance.AeroplaneHighScore = m_CurrentScore;
					} else {
						GameManager.Instance.GunShootingHighScore = m_CurrentScore;
					}
					UIManager.Instance.SetMessageText ("Finish!", Color.red, true);
					Invoke ("SceneChange", 3f);
				}
			}
		}
		UIManager.Instance.SetCurrentTime (m_CurrentTime);
		UIManager.Instance.SetScore (m_CurrentScore);
	}

	public void SetScore(float value){
		if (m_IsTimeCount) {
			m_CurrentScore += value;
		}
	}

	void SceneChange(){
		GameManager.Instance.SceneChange (m_StageScelectSceneName);
	}


}
