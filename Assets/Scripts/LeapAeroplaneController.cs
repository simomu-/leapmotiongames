﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Aeroplane;

public class LeapAeroplaneController : MonoBehaviour {

	[SerializeField] LeapHandInputManager m_LeapInputR;
	[SerializeField] AeroplaneUserControl2Axis m_AeroplaneControl;
	[SerializeField] float m_GetScore;
	[SerializeField] GameObject m_PointPrefab;
	[SerializeField] float m_PointCreateInterval;
	[SerializeField] float m_CreateSpace;
	[SerializeField] float m_MaxHeight;

	void Start(){
		InvokeRepeating ("CreatePoint",m_PointCreateInterval,m_PointCreateInterval);
		GameManager.Instance.RecentPlayStageIndex = 0;
	}

	void Update(){
		m_AeroplaneControl.Roll = m_LeapInputR.GetLeapHorizontalAxis ();
		m_AeroplaneControl.Pitch = m_LeapInputR.GetLeapVerticalAxis ();
		m_AeroplaneControl.AirBrakes = m_LeapInputR.GetFingerDown (1);
	}

	void CreatePoint(){
		Vector3 position = 
			new Vector3 (
				Random.Range ( m_CreateSpace / -2f, m_CreateSpace / 2f),
				Random.Range (20f, m_MaxHeight), 
				Random.Range (m_CreateSpace / -2f, m_CreateSpace / 2f));
		Instantiate (m_PointPrefab, position, Quaternion.identity);
	}

	/*
	void OnTriggerEnter(Collider _other){
		ScoreCounter.Instance.SetScore (m_GetScore);
		_other.enabled = false;
		Destroy (_other.transform.root.gameObject);
		//DestroyImmediate (_other.transform.root.gameObject);
	}
	*/

}
