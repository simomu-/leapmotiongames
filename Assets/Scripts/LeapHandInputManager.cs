﻿using UnityEngine;
using System.Collections;
using Leap;
using Leap.Unity;

public class LeapHandInputManager : MonoBehaviour {

	[SerializeField] IHandModel m_IHandModel;
	readonly float m_FlipThreshold = 1.5f;
	readonly float m_FingerDownThreshold = 0.005f;

	bool m_PreFrameUpFliping = false;
	bool m_PreFrameDownFliping = false;
	bool m_PreFrameRightFliping = false;
	bool m_PreFrameLeftFliping = false;
	bool m_PreFrameForwardFliping = false;
	bool m_PreFrameBackFliping = false;

	bool m_PreFrameOnPointStart = false;
	bool m_PreFrameOnPointEnd = false;

	public IHandModel iHandModel{
		get{
			return m_IHandModel;
		}
	}

	public float GetLeapHorizontalAxis(){
		Hand hand = m_IHandModel.GetLeapHand ();
		if (hand == null || !m_IHandModel.gameObject.activeInHierarchy) {
			return 0f;
		}
		Vector3 handEularnAngle = hand.Rotation.ToQuaternion ().eulerAngles;
		float result = 0;
		if (handEularnAngle.z >= 0 && handEularnAngle.z < 180) {
			result = Mathf.Clamp01 (handEularnAngle.z / 30f) * -1;
		}
		if (handEularnAngle.z >= 180 && handEularnAngle.z <= 360) {
			result = Mathf.Clamp01 ((360f - handEularnAngle.z) / 30f);
		}
		if (Mathf.Abs (result) <= 0.05f) {
			result = 0;
		}
		return result;
	}

	public float GetLeapVerticalAxis(){
		Hand hand = m_IHandModel.GetLeapHand ();
		if (hand == null || !m_IHandModel.gameObject.activeInHierarchy) {
			return 0f;
		}
		Vector3 handEularnAngle = hand.Rotation.ToQuaternion ().eulerAngles;
		float result = 0;
		if (handEularnAngle.x >= 0 && handEularnAngle.x < 180) {
			result = Mathf.Clamp01 (handEularnAngle.x / 30f) ;
		}
		if (handEularnAngle.x >= 180 && handEularnAngle.x <= 360) {
			result = Mathf.Clamp01 ((360f - handEularnAngle.x) / 30f) * -1;
		}
		if (Mathf.Abs (result) <= 0.05f) {
			result = 0;
		}
		return result;
	}

	public bool OnFlip(FlipDirection direction){
		Hand hand = m_IHandModel.GetLeapHand ();
		if (hand == null || !m_IHandModel.gameObject.activeInHierarchy) {
			return false;
		}
		//Finger finger = hand.Fingers [1];
		//Vector3 velocity = finger.TipVelocity.ToVector3 ();
		Vector3 velocity = Vector3.zero;
		for(int i = 1; i < hand.Fingers.Count; i++){
			velocity += hand.Fingers [i].TipVelocity.ToVector3 ();
		}
		velocity /= 4f;

		bool up = false;
		bool down = false;
		bool right = false;
		bool left = false;
		bool forward = false;
		bool back = false;

		switch (direction) {
		case FlipDirection.Right:
			if (velocity.x >= m_FlipThreshold) {
				right = true;
				bool result = !m_PreFrameRightFliping && right;
				m_PreFrameRightFliping = right;
				return result;
			} else {
				m_PreFrameRightFliping = false;
			}
			break;
		case FlipDirection.Left:
			if (velocity.x <= -m_FlipThreshold) {
				left = true;
				bool result = !m_PreFrameLeftFliping && left;
				m_PreFrameLeftFliping = left;
				return result;
			} else {
				m_PreFrameLeftFliping = false;
			}
			break;
		case FlipDirection.Up:
			if (velocity.y >= m_FlipThreshold) {
				up = true;
				bool result = !m_PreFrameUpFliping && up;
				m_PreFrameUpFliping = up;
				return result;
			} else {
				m_PreFrameUpFliping = false;
			}
			break;
		case FlipDirection.Down:
			if (velocity.y <= -m_FlipThreshold) {
				down = true;
				bool result = !m_PreFrameDownFliping && down;
				m_PreFrameDownFliping = down;
				return result;
			} else {
				m_PreFrameDownFliping = false;
			}
			break;
		case FlipDirection.Forward:
			if (velocity.z <= m_FlipThreshold) {
				forward = true;
				bool result = !m_PreFrameForwardFliping && forward;
				m_PreFrameForwardFliping = forward;
				return result;
			} else {
				m_PreFrameForwardFliping = false;
			}
			break;
		case FlipDirection.Back:
			if (velocity.z <= -m_FlipThreshold) {
				back = true;
				bool result = !m_PreFrameBackFliping && back;
				m_PreFrameBackFliping = back;
				return result;
			} else {
				m_PreFrameBackFliping = false;
			}
			break;
		}
		return false;
	}

	public bool OnPoint(out Vector3 outPositionOnScreen){
		Hand hand = m_IHandModel.GetLeapHand ();
		if (hand == null || !m_IHandModel.gameObject.activeInHierarchy) {
			outPositionOnScreen = Vector3.zero;
			return false;
		}
		Vector3[] directions = new Vector3[hand.Fingers.Count];
		for (int i = 0; i < hand.Fingers.Count; i++) {
			directions[i] = hand.Fingers [i].Direction.ToVector3 ();
		}
		bool result = false;
		if (directions[1].z > 0 && directions[2].z < 0 && directions[3].z < 0 && directions[4].z < 0) {
			result = true;
		}
		Vector3 fingerPosition = hand.Fingers [1].TipPosition.ToVector3 ();
		Camera camera = transform.root.gameObject.GetComponent<Camera> ();
		outPositionOnScreen = camera.WorldToScreenPoint (fingerPosition);
		//m_PreFrameOnPoint = result;
		return result;
	}

	public bool OnPointStart(){
		Vector3 pos;
		bool result = OnPoint (out pos) && !m_PreFrameOnPointStart;
		m_PreFrameOnPointStart = OnPoint(out pos);
		return result;
	}

	public bool OnPointEnd(){
		Vector3 pos;
		bool result = !OnPoint (out pos) && m_PreFrameOnPointEnd;
		m_PreFrameOnPointEnd = OnPoint(out pos);
		return result;
	}

	public bool GetFingerDown(int fingerIndex){
		Hand hand = m_IHandModel.GetLeapHand ();
		bool result = true;
		if (hand == null || !m_IHandModel.gameObject.activeInHierarchy) {
			return false;
		}
			
		for (int i = 0; i < hand.Fingers.Count; i++) {
			if (i == fingerIndex) {
				continue;
			}
			if ((hand.Fingers [i].TipPosition.ToVector3 ().y - hand.Fingers [fingerIndex].TipPosition.ToVector3 ().y) <= m_FingerDownThreshold) {
				result = false;
				break;
			}
		}
		return result;

	}

	public Vector3 GetFingerPosition(int fingerIndex){
		Hand hand = m_IHandModel.GetLeapHand ();
		if (hand == null || !m_IHandModel.gameObject.activeInHierarchy) {
			return Vector3.zero;
		}
		Camera camera = transform.root.GetComponent<Camera> ();
		return camera.WorldToScreenPoint (hand.Fingers [fingerIndex].TipPosition.ToVector3 ());
	}
}
	