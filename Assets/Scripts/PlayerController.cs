﻿using UnityEngine;
using System.Collections;
using Leap;
using Leap.Unity;

public class PlayerController : MonoBehaviour {

	[SerializeField] IHandModel m_IHandModel;
	[SerializeField] LeapHandInputManager m_InputManager;
	[SerializeField] float m_Speed;
	[SerializeField] float m_Turn;
	Rigidbody m_Rigidbody;
	Vector3 m_FirstHandPosition;

	void Awake(){
		m_Rigidbody = GetComponent<Rigidbody> ();
	}

	void Update(){
		Hand hand = m_IHandModel.GetLeapHand ();
		if (hand != null && m_IHandModel.gameObject.activeInHierarchy) {
			
			//m_Rigidbody.AddForce (transform.up * hand.PalmPosition.ToVector3 ().y * 100, ForceMode.Force);
			//m_Rigidbody.AddForce (transform.forward * hand.PalmPosition.ToVector3 ().z * 10, ForceMode.Force);
			//m_Rigidbody.MoveRotation (Quaternion.Euler (transform.up * m_InputManager.GetLeapHorizontalAxis(hand) * m_Turn * Time.deltaTime));
			m_Rigidbody.AddForce (transform.forward * m_Speed * Time.deltaTime, ForceMode.Impulse);
			transform.Rotate (transform.up * m_InputManager.GetLeapHorizontalAxis () * m_Turn * Time.deltaTime);
			//transform.Rotate (transform.right * m_InputManager.GetLeapVerticalAxis (hand) * m_Turn * Time.deltaTime);
			if (m_InputManager.OnFlip (FlipDirection.Down)) {
				Debug.Log ("Fire");
			}
		} else {
			Debug.Log ("DisableHand");
		}
	}

}
