﻿using UnityEngine;
using System.Collections;

public class LeapTurretControl : MonoBehaviour {

	[SerializeField] LeapHandInputManager m_LeapInputR;
	[SerializeField] Transform m_Turret;
	[SerializeField] float m_Range;
	[SerializeField] float m_ReloadTime;
	[SerializeField] float m_TurretRotateSpeed;
	[SerializeField] Transform m_Reticle;
	[Space]
	[SerializeField] GameObject m_ExplosionPrefab;

	bool m_IsFire = true;
	bool m_IsNowFireing = false;
	LineRenderer m_LineRenderer;
	AudioSource m_AudioSource;

	void Start(){
		m_LineRenderer = GetComponent<LineRenderer> ();
		m_AudioSource = GetComponent<AudioSource> ();
		GameManager.Instance.RecentPlayStageIndex = 2;
	}

	void Update(){
		Ray ray = Camera.main.ScreenPointToRay (m_LeapInputR.GetFingerPosition (2));

		m_Turret.rotation = 
			Quaternion.Slerp (m_Turret.rotation,
			Quaternion.LookRotation (ray.GetPoint (m_Range) - m_Turret.position), m_TurretRotateSpeed * Time.deltaTime);
		m_Reticle.position = Camera.main.WorldToScreenPoint (new Ray (m_Turret.position, m_Turret.forward).GetPoint (m_Range));

		if (m_LeapInputR.GetFingerDown (1) && m_IsFire) {
			m_AudioSource.Play ();
			m_IsFire = false;
			Invoke ("ResetFireInvoke", m_ReloadTime);
			m_IsNowFireing = true;
			Invoke ("StopFireing", 0.01f);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, m_Range)) {
				if (hit.transform.tag == "Character") {
					Destroy (hit.transform.gameObject);
					Instantiate (m_ExplosionPrefab, hit.point, Quaternion.identity);
					ScoreCounter.Instance.SetScore (hit.transform.position.z - transform.position.z);
				}
			}
		}
		m_LineRenderer.SetPosition (0, m_Turret.position);
		m_LineRenderer.SetPosition (1, m_IsNowFireing ? ray.GetPoint(m_Range) : m_Turret.position);
		/*
		if (m_IsNowFireing) {
			m_LineRenderer.SetPosition (0, m_Turret.position);
			m_LineRenderer.SetPosition (1, ray.GetPoint (m_Range));
		}else{
			m_LineRenderer.SetPosition (0, m_Turret.position);
			m_LineRenderer.SetPosition (1, m_Turret.position);
		}*/
	}

	void StopFireing(){
		m_IsNowFireing = false;
	}

	void ResetFireInvoke(){
		m_IsFire = true;
	}

}
