﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Leap;
using Leap.Unity;

public class Test : MonoBehaviour {

	[SerializeField] IHandModel m_RightHand;
	[SerializeField] IHandModel m_LeftHand;
	[SerializeField] Transform m_PointUI;
	[SerializeField] GraphicRaycaster m_Raycaster;
	[SerializeField] readonly float m_FlipThreshold = 1.2f;

	StageSelectScroll m_StageUIControll;
	PointerEventData m_EventData;
	List<RaycastResult> m_RaycastResults;

	bool m_RightHandFlipHappen = false;

	bool m_PreFrameUpFliping = false;
	bool m_PreFrameDownFliping = false;
	bool m_PreFrameRightFliping = false;
	bool m_PreFrameLeftFliping = false;

	void Start(){
		m_StageUIControll = GetComponent<StageSelectScroll> ();
	}

	void Update(){
		Hand hand = m_RightHand.GetLeapHand ();
		if (hand != null) {
			if (OnFlip (hand, FlipDirection.Right)) {
				m_StageUIControll.MoveLeft ();
			}
			if (OnFlip (hand, FlipDirection.Left)) {
				m_StageUIControll.MoveRight ();
			}
			Vector3 fingerPos;
			if (OnPoint (hand, out fingerPos)) {
				m_PointUI.gameObject.SetActive (true);
				m_PointUI.position = fingerPos;
				FingerInputModule.IsPoint = true;
				FingerInputModule.FingerPosition = fingerPos;
			} else {
				FingerInputModule.IsPoint = false;
				m_PointUI.gameObject.SetActive (false);
			}
			//Debug.Log (hand.Fingers [2].Direction.ToVector3());
		}
	}



	void LateUpdate(){
		//Debug.Log (m_PreFrameRightFliping);
		m_RightHandFlipHappen = false;
	}

	public FlipDirection GetFlip(Finger finger){
		Vector3 velocity = finger.TipVelocity.ToVector3 ();
		FlipDirection direction = FlipDirection.None;
		if (!m_RightHandFlipHappen) {
			m_RightHandFlipHappen = true;
			if (velocity.x <= -m_FlipThreshold) {
				direction = FlipDirection.Left;
			}
			if (velocity.x >= m_FlipThreshold) {
				direction = FlipDirection.Right;
			}
			if (velocity.y >= m_FlipThreshold) {
				direction = FlipDirection.Up;
			}
			if (velocity.y <= -m_FlipThreshold) {
				direction = FlipDirection.Down;
			}
		}
		return direction;
	}

	public bool OnFlip(Hand hand,FlipDirection direction){
		Finger finger = hand.Fingers [2];
		Vector3 velocity = finger.TipVelocity.ToVector3 ();
		bool up = false;
		bool down = false;
		bool right = false;
		bool left = false;
		switch (direction) {
		case FlipDirection.Right:
			if (velocity.x >= m_FlipThreshold) {
				right = true;
				bool result = !m_PreFrameRightFliping && right;
				m_PreFrameRightFliping = right;
				return result;
			} else {
				m_PreFrameRightFliping = false;
			}
			break;
		case FlipDirection.Left:
			if (velocity.x <= -m_FlipThreshold) {
				left = true;
				bool result = !m_PreFrameLeftFliping && left;
				m_PreFrameLeftFliping = left;
				return result;
			} else {
				m_PreFrameLeftFliping = false;
			}
			break;
		case FlipDirection.Up:
			if (velocity.y >= m_FlipThreshold) {
				up = true;
				bool result = !m_PreFrameUpFliping && up;
				m_PreFrameUpFliping = up;
				return result;
			} else {
				m_PreFrameUpFliping = false;
			}
			break;
		case FlipDirection.Down:
			if (velocity.y <= -m_FlipThreshold) {
				down = true;
				bool result = !m_PreFrameDownFliping && down;
				m_PreFrameDownFliping = down;
				return result;
			} else {
				m_PreFrameDownFliping = false;
			}
			break;
		}
		return false;
	}

	public bool OnPoint(Hand hand,out Vector3 outpositionOnScreen){
		//Finger[] fingers = new Finger[hand.Fingers.Count];
		Vector3[] directions = new Vector3[hand.Fingers.Count];
		for (int i = 0; i < hand.Fingers.Count; i++) {
			directions [i] = hand.Fingers [i].Direction.ToVector3 ();
		}
		bool result = false;
		if (directions [1].z > 0 && directions [2].z < 0 && directions [3].z < 0 && directions [4].z < 0) {
			result = true;
		}
		Vector3 fingerPosition = hand.Fingers [1].TipPosition.ToVector3 ();
		outpositionOnScreen = Camera.main.WorldToScreenPoint (fingerPosition);
		return result;
		
	}

}
