﻿using UnityEngine;
using System.Collections;

public class EnemyRotater : MonoBehaviour {

	Vector3 m_NewRotation;
	bool m_IsRotate = false;

	void Start(){
		m_NewRotation = transform.eulerAngles +
			new Vector3 (Random.Range (-45f, 45f), Random.Range (-45f, 45f), Random.Range (-45f, 45f));

		Invoke ("EnemyRotate", 1.5f);
	}

	void Update(){
		if(m_IsRotate){
			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.Euler(m_NewRotation),Time.deltaTime);
		}
	}

	void EnemyRotate(){
		m_IsRotate = true;
	}


}
