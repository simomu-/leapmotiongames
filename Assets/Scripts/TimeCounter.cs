﻿using UnityEngine;
using System.Collections;

public class TimeCounter : MonoBehaviour {

	readonly string m_StageSelectSceneName = "GameSelect";

	float m_CurrentTime = 0;
	bool m_IsTimeCount = true;

	void Update(){
		if (m_IsTimeCount) {
			m_CurrentTime += Time.deltaTime;
		}
		UIManager.Instance.SetCurrentTime (m_CurrentTime);
	}

	void OnTriggerEnter(Collider _other){
		if (_other.transform.root.tag == "Player") {
			if (m_IsTimeCount) {
				UIManager.Instance.SetMessageText ("Goal!", Color.red, true);
				m_IsTimeCount = false;
				GameManager.Instance.CarHighScore = m_CurrentTime;
				Invoke ("SceneChange", 3f);
			}
		}
	}

	void SceneChange(){
		GameManager.Instance.SceneChange (m_StageSelectSceneName);
	}

}
