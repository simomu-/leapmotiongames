﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	[SerializeField] GameObject m_EnemyPrefab;
	[SerializeField] float m_SpawnInterval;
	[SerializeField] Vector3 m_CenterOfCreateField;
	[SerializeField] float m_MaxFieldSize;

	void Start(){
		InvokeRepeating ("EnemySpawn", m_SpawnInterval, m_SpawnInterval);
	}

	void EnemySpawn(){
		Vector3 position = 
			m_CenterOfCreateField +
			new Vector3 (
				Random.Range (m_MaxFieldSize / -2f, m_MaxFieldSize / 2f),
				Random.Range (m_MaxFieldSize / -2f, m_MaxFieldSize / 2f),
				Random.Range (m_MaxFieldSize / -2f, m_MaxFieldSize / 2f));
		GameObject enemy = Instantiate (m_EnemyPrefab, position, Quaternion.Euler (0, 180f, 0)) as GameObject;
		enemy.GetComponent<ConstantForce> ().relativeForce = Vector3.forward * Random.Range (10f, 20f);
		Destroy (enemy, 10f);
	}

}
