﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DemoTest : MonoBehaviour {

	[SerializeField] Text[] m_Text;
	[SerializeField] LeapHandInputManager m_Manager;

	void Update(){
		m_Text [0].text = "Finger 1:" + m_Manager.GetFingerDown (1);
		m_Text [1].text = "Finger 2:" + m_Manager.GetFingerDown (2);
		m_Text [2].text = "Finger 3:" + m_Manager.GetFingerDown (3);
		m_Text [3].text = "Finger 4:" + m_Manager.GetFingerDown (4);
		//m_Text [1].text = "Vertical:" + m_Manager.GetLeapVerticalAxis ();
	}


}
